
public class WithoutMultiplications {
    public static void main(String[] args) {
        long timeBefore = System.nanoTime();
        int operandA = 204;
        int operandB = -10;
        int result;

        if (isAGreaterThanB(operandA,operandB)){
            result = multiplications(operandA,operandB);
        }else {
            result = multiplications(operandB,operandA);
        }

        System.out.println(operandA + " * " + operandB + " = " + result + "\n Затраченное время - " + (System.nanoTime() - timeBefore));
    }

    private static boolean isAGreaterThanB(int operandA, int operandB) {
        return module(operandA) > module(operandB);
    }

    private static int module(int a) {
        return (a < 0) ? -a : a;
    }

    private static int multiplications(int operandA, int operandB) {
        int result = 0;
        boolean isLessThanZero = false;
        if ((operandA < 0 || operandB < 0) && !(operandA < 0 && operandB < 0)){
            isLessThanZero = true;
        }

        operandA = module(operandA);
        operandB = module(operandB);


        for (int i = 0; i < operandB; i++) {
            result += operandA;
        }

        if (!isLessThanZero) {
            return result;
        }
        return -result;
    }
}
